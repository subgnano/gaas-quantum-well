# GaAs Quantum Well

Solves the GaAS quantum well within the Hartree aproximation and calculate the spin-orbit couplings.

## Description of the main parameters

<img src="images/qw.png" width=300 />

As illustrated in the figure above, the geometric parameters of the quantum well are:

- Lc [nm] : width of the main quantum well
- Ls [nm] : spacer from the well to the delta-doping region
- Ld [nm] : extra buffer after the delta-doping region
- Lb [nm] : width of the barriers within the main well
- cbars : `None` or list `[z0, z1, ...]` of the barriers positions.

All these parameters are set on the `init` function of the `hartree` class. Additionally, the initialization allow you to inform:

- eps : the dielectric constant (default: 12.9 for GaAs)
- efmass: the effective mass (default: 0.067 for GaAs)
- nt [$`10^{11}`$ cm$`^{-2}`$] : the total electron areal density
- ndop [$`10^{11}`$ cm$`^{-2}`$] : the delta-doping areal density
- Nz : number of points to discretize the z axes.

The input densities are in $`[10^{11}`$ cm$`^{-2}`$ because it is the unit mostly used by experimentalists, but the output densities are written in nm$`^{-2}`$.

## The model

The code solves the Schrödinger equation self-consistently with the Hartree potential, assuming that on the (x,y) plane the electron gas is free.

**Schrödinger equation**

```math
\left[-\dfrac{\hbar^2}{2m^*}\dfrac{\partial^2}{\partial z^2} + V(z) + V_H(z) - eF z\right]\varphi(z) = \varepsilon \varphi(z)
```

Here $`m^* = m_{eff} m_0`$, V(z) is the structural potential, eF is an electrical field applied along z, and the Hartree potential is given by the Poisson equation

```math
\dfrac{\partial^2}{\partial z^2}V_H(z) = -\dfrac{4\pi e^2}{\epsilon} \rho(z)
```

Here $`\epsilon`$ is the dielectric constant, and $`\rho(z)`$ is the electron density (per volume),

```math
\rho(z) = \sum_{j} n_{2D,j} |\varphi_j(z)|^2
```

where $`\varphi_j(z)`$ is the envelope function of the j-th subband with energy $\varepsilon_j$, given by the solutions of the Schrödinger equation above, and

```math
n_{2D,j} = N_0 \int_{\varepsilon_j}^\infty f_\mu(\varepsilon) d\varepsilon
```

is the areal density of occupied states on subband j, $`N_0 = m/(\pi \hbar^2)`$ is the 2DEG density of states and the Fermi distribution at temperature $`\beta^{-1} = k_BT`$ and chemical potential $\mu$ is

```math
f_\mu(\varepsilon) = \dfrac{1}{1+\exp[-\beta(\varepsilon-\mu)]}
```

At zero temperature, the occupation factor simplifies to

```math
n_{2D,j} = N_0 (\varepsilon_F - \varepsilon_j)
```

where $`\varepsilon_F`$ is the Fermi energy (zero temperature limit of the chemical potential).

## The spin-orbit couplings

The spin-orbit couplings (SOC) are calculated following the reference

> **Spin-orbit interaction in GaAs wells: From one to two subbands**
>
> Jiyong Fu and J. Carlos Egues, [Phys. Rev. B 91, 075408 (2015)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.91.075408)

**TO DO:** write down the main expressions to calculate the SOCs here

## Calling the library

The library works by running two main functions:

**The initialization**

The `__init__(...)` function creates the model, its parameters are defined above. Call it by running

```python
import hartreeclass as ht
model = ht.hartree(nt=8.6, ndop=0, eps=12.9, efmass=0.067, Nz=100, Lc=26, Ls=12, Ld=2, Lb=0, cbars=None)
```

This will init and return the `model` object with the indicated parameters. All parameters have default values and can be omitted if the default values are adequated.

**Run the simulation**

To run the main calculation, call the `runHartree` method as

```python
z, v0, vh, vg, evals, evecs, mu, it, rashba, dress1, dress3, dens = model.runHartree(mix=0.5, nit=2000, eF=0, fixmu=None, subbands=2)
```

The *input parameters* are:

- mix : how fast to mixture the old and new Hartree potential within the self-consistent cycle and slow down the convergence to improve precision. 
- nit : maximum number of self-consistent iterations.
- eF [meV/nm] : the electric field applied along z
- fixmu : if `None` runs the code with constant density and calculates $`\mu=\varepsilon_F`$ self-consistently. Or inform $`\mu`$ [meV] to run with constant chemical potential.
- subbands : number of subbands to consider when calculating SOC and density = max(occ, subbands), where occ is the number of occupied subbands. If subbands > occ, pads density with zeros.

PS: since the current version is written at zero temperature (T=0), we should call $`\mu=\varepsilon_F`$ the Fermi energy instead of chemical potential. 

The *output parameters* are

- z [nm] : (array) the discrete z axes
- v0 [meV] : (array) the structural potential of the quantum well V0(z)
- vh [meV] : (array) the Hartree potential Vh(z)
- vg [meV]: (array) the electric field potential, Vg(z) = -eFz
- evals [meV]: (array) the eigenvalues $`\varepsilon_j`$ = evals[j]
- evecs : (matrix) the eigenvectors $`\varphi_j(z)`$ = evecs[:,j]
- mu [meV] : the chemical potential
- it : number of iterations used to achieve convergence
- rashba [meV nm]: (matrix) intra- and inter-subband Rashba SOC
- dress1[meV nm]: (matrix) intra- and inter-subband linear Dresselhaus SOC
- dress3 [meV nm]: (array) intra-subband cubic Dresselhaus SOC
- dens [1/nm²] : (array) electron density on each subband

## The atomic units parameters are always useful

Our numerical implementation uses the Rydberg energy and Bohr radius,

$`R_y = \dfrac{me^4}{2\hbar^2} = 13.605`$ eV

$`a_0 = \dfrac{\hbar^2}{me^2} = 0.0529`$ nm

These can be combined to form

$`\dfrac{\hbar^2}{2m} = R_y a_0^2`$

$`e^2 = 2R_ya_0`$