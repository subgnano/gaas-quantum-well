# in a module, try to import only
# the relevant functions from each library
import numpy as np

class hartree:
    #############################################################
    # CLASS INITIALIZATION
    # default parameters
    '''
            nt : total charge [10¹¹/cm^2]
          ndop : doping density [10¹¹/cm^2]
           eps : dielectric constant
        efmass : effective mass
            Nz : number of points along z
            Lc : central well width [nm]
            Ls : side barrier [nm] - side barrier up to delta-doping region
            Ld : dopping buffer [nm] - buffer after the delta-doping region
            Lb : barrier width [nm]
         cbars : None or list of barrier centers [nm]
    '''
    #############################################################
    def __init__(self, nt=8.6, ndop=0, eps=12.9, efmass=0.067, Nz=100, Lc=26, Ls=12, Ld=2, Lb=0, cbars=None):
        # --------------------------------
        #### constants
        #--------------------------------
        self.Ry = 13.6056917253e3  # meV
        self.a0 = 0.0529177208319  # nm
        self.hbar = 0.65821188926  # meV.ps
        # --------------------------------
        #### parameters
        # --------------------------------
        # total charge converting to [1/nm^2]
        self.nt = 1e-3*nt
        # dielectric constant
        self.eps = eps
        # effective mass [dimensionless]
        self.efmass = efmass
        # number of points to discretize z
        self.Nz = Nz
        # lenghts [nm]
        self.Lc = Lc # central region
        self.Ls = Ls # side barrier
        self.Ld = Ld # dopping buffer
        self.Lb = Lb # barriers widht
        # list of barriers center [nm]
        self.cbars = [] # init as new empty list
        if cbars is not None: # append list elements if not empty
            for cbar in cbars:
                self.cbars.append(cbar)


        # --------------------------------
        # composed parameters
        # --------------------------------
        # hbar^2/2m [meV nm^2]
        self.h2m = self.Ry*(self.a0**2)/efmass
        self.h2m0 = self.Ry*(self.a0**2)
        # mass [meV ps² / nm² ]
        #self.m = efmass*(self.hbar**2)/(2*self.Ry*self.a0**2)
        # charge^2
        self.e2 = 2*self.Ry*self.a0
        # 2D DOS [1/meVnm^2]
        self.DOS = 1.0/(2*np.pi*self.h2m)
        # symmetric delta dopping converting units
        self.ndop = 1e-3*ndop
        self.vq = self.ndop*4*np.pi*self.e2/self.eps

        # --------------------------------
        # material parameters
        # --------------------------------
        # GaAs
        self.Eg = 1.519e3 # meV
        self.Dg = 0.341e3 # meV
        self.Ep = 28.8e3 # meV
        # AlAs
        self.Eg2 = 3.099e3 # meV
        self.Dg2 = 0.280e3 # meV
        self.Ep2 = 21.1e3 # meV
        # AlxGa1-xAs
        self.x = 0.3
        self.CBO = 0.65
        self.Egx = self.Eg2*self.x + self.Eg*(1-self.x)
        self.Dgx = self.Dg2*self.x + self.Dg*(1-self.x)
        # gaps
        self.d6 = (self.Egx-self.Eg)*self.CBO
        self.d8 = self.Egx-self.Eg-self.d6
        self.d7 = self.d8 + self.Dgx - self.Dg

        # --------------------------------
        #### END INIT
        # --------------------------------

    ##########################################################
    # CALCLUATES THE POTENTIAL AND Z AXIS
    ##########################################################
    def potential(self):
        # from self to local
        Lc = self.Lc
        Ls = self.Ls
        Ld = self.Ld
        Nz = self.Nz
        Lb = self.Lb

        # main sizes
        Lz = Lc + 2*(Ls+Ld)
        z = np.linspace(-Lz/2, Lz/2, Nz)
        # and step
        dz = z[1]-z[0]

        # well shaped function
        well = lambda z, c, w, s: (np.tanh((z-c-w/2)/s) + np.tanh((-(z-c)-w/2)/s))/2

        # creates main well
        v0 = self.d6 * well(z, 0, Lc, dz)
        # adds barriers
        Nb = len(self.cbars)
        for i in range(Nb):
            v0 += -self.d6 * well(z, self.cbars[i], Lb, dz)

        # adds the delta-dopping
        zq = 0.5*Lc + Ls
        v0 = v0 + 0.5 * self.vq * (np.abs(z-zq) + np.abs(z+zq) - 2*zq)

        return z, v0


    ##########################################################
    # CALCLUATES THE K2 matrices
    '''
        z: axis [nm]
    '''
    ##########################################################
    def k2matrix(self, z):
        Nz = len(z)
        dz = z[1]-z[0]
        k2 = (2*np.eye(Nz) - np.eye(Nz, k=1) - np.eye(Nz, k=-1))/(dz**2)
        return k2


    ##########################################################
    # SELF-CONSISTENT CICLE
    '''
        mix: mixture of old and new Hartree potentials
        nit: number of iterations
        eF: electric field [meV/nm]
        fixmu: value for the constant chemical potential, fixed density if None
        subbands: number of subbands to consider on SOC calculation
    '''
    ##########################################################
    def runHartree(self, mix=0.5, nit=2000, eF=0, fixmu=None, subbands=2):
        # from self to local
        Nz = self.Nz

        # build potential
        z, v0 = self.potential()
        # total lenght
        Lz = z[-1] - z[0]
        # step size
        dz = z[1]-z[0]
        # electric field
        vg = -eF*(z+Lz/2)
        # and k2 matrix
        k2 = self.k2matrix(z)

        # initialize vectors and matrices
        vh = np.zeros(Nz) # Hartree potential
        delist = [] # list of DeltaE for each iteration

        # loop
        for it in range(nit+1):

            # define Hamiltonian
            h = self.h2m*k2 + np.diag(v0 + vh + vg)
            # solve it
            evals, evecs = np.linalg.eigh(h)

            # add DeltaE to the list
            delist.append(evals[2]-evals[1])

            if fixmu == None:
                # calculate the Fermi energy mu and occupation with fixed density
                nocc = 1 # initial guess 1 subband
                mu = ( self.nt/self.DOS + np.sum(evals[:nocc]) )/nocc
                while nocc < Nz and mu >= evals[nocc]:
                    nocc += 1
                    mu = ( self.nt/self.DOS + np.sum(evals[:nocc]) )/nocc
            else:
                # calculates occupation with fixed mu
                mu = fixmu
                nocc = sum(evals<mu)

            # normalize evecs to up nocc or subbands
            # to guarantee there's enough normalized evecs 
            # for the SOC calculation
            for ie in range(np.max([nocc, subbands])):
                nrm = sum(evecs[:,ie]**2)*dz
                evecs[:,ie] /= np.sqrt(nrm)

            # calculate density
            vdens = self.DOS * (mu - evals[:nocc]) # n2d per subband
            dens = np.zeros(Nz) # total n3d
            for ie in range(nocc):
                dens += vdens[ie]*evecs[:,ie]**2

            # Hartree update
            vnew = (4*np.pi*self.e2/self.eps)*np.linalg.solve(k2, dens)
            vh = mix * vnew + (1-mix) * vh

            # check convergence
            if it > 15:
                sdev = np.std(delist[(-10):])
                if sdev < 1e-8:
                    break
        # end loop
        if sdev >= 1e-8:
            print('\n\n--------- Full loop:', nit, ', deviation:', sdev, ', eF:', eF, '\n\n')

        # calculate SOC
        #------------------------------------------
        def del2(v):
            kernel = np.array([1.0, -2.0, 1.0])
            res = np.convolve(kernel, v)
            return res[1:-1]

        P = np.sqrt(self.Ep * self.h2m0) # Kane
        etaw = ((P**2)/3)*(self.d8/self.Eg**2 - self.d7/(self.Eg+self.Dg)**2)/self.d6
        etah = ((P**2)/3)*(1/self.Eg**2 - 1/(self.Eg+self.Dg)**2)
        gamma0 = 11.0 # meV nm^3 = eV A^3

        # number of subbands to consider on the SOC calculation
        N = np.max([nocc, subbands])
        # calculates rashba and linear dresslhaus
        rashba = np.zeros((N, N))
        dress1 = np.zeros((N, N))
        aux = etaw * np.gradient(v0)/dz - etah*np.gradient(vh + vg)/dz
        for i in range(N):
            for j in range(N):
                rashba[i, j] = np.sum(evecs[:,i]*aux*evecs[:,j])*dz
                dress1[i, j] = -gamma0 * np.sum(evecs[:,i]*del2(evecs[:,j])/dz**2)*dz
        # calculates cubic dresselhaus
        kf = np.sqrt(2*np.pi*vdens)
        if len(kf) < N:
            vdens = np.pad(vdens, (0,N-len(kf)), constant_values=0)
            kf    = np.pad(kf   , (0,N-len(kf)), constant_values=0)
        dress3 = gamma0*(kf**2)/4

        #------------------------------------------
        return z, v0, vh, vg, evals, evecs, mu, it, rashba, dress1, dress3, vdens














